
box_cleanslate variant for box_grey theme
-----------------------------------------

This is a theme variation, it uses the box_grey tpl files, but a seperate stylsheet and logo.

This style is essentially the phptemplate cleanslate theme moved onto the box_grey template. It's not identical to cleanslate but uses the same colours and similar borders.

Author
------
adrinux
mailto: adrinux@perlucida.com
IM: perlucida

Known Problems
--------------
See the box_grey README
