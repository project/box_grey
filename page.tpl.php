<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>
<body class="<?php print $body_classes; ?>">
<div id="header" class="clear-block">
  <?php print $search_box ?>
  <?php if ($logo) : ?>
  <a href="<?php print url() ?>" title="Index Page"><img src="<?php print($logo) ?>" alt="Logo" /></a>
  <?php endif; ?>
  <?php if ($site_name) : ?>
    <h1 id="site-name"><a href="<?php print url() ?>" title="Index Page"><?php print($site_name) ?></a></h1>
  <?php endif;?>
  <?php if ($site_slogan) : ?>
    <span id="site-slogan"><?php print($site_slogan) ?></span>
  <?php endif;?>
  <?php print $header ?>
</div>
<div id="top-nav">
  <?php if (!empty($secondary_links)): ?>
    <?php print theme('links', $secondary_links, array('id' => 'secondary')); ?>
  <?php endif; ?>
  <?php if (!empty($primary_links)): ?>
    <?php print theme('links', $primary_links, array('id' => 'primary')); ?>
  <?php endif; ?>
</div>
<table id="content">
  <tr>
  <?php if (!empty($left)): ?>
      <td class="sidebar" id="sidebar-left">
        <?php print $left ?>
      </td>
    <?php endif; ?>
        <td class="main-content" id="content-<?php print $layout ?>">
        <?php if (!empty($title)): ?>
          <h2 class="content-title"><?php print $title ?></h2>
        <?php endif; ?>
        <?php if (!empty($tabs)): ?>
          <?php print $tabs ?>
        <?php endif; ?>

        <?php if (!empty($mission)): ?>
          <div id="mission"><?php print $mission ?></div>
        <?php endif; ?>

        <?php if (!empty($help)): ?>
          <p id="help"><?php print $help ?></p>
        <?php endif; ?>

        <?php if ($show_messages && $messages): ?>
          <div id="message"><?php print $messages ?></div>
        <?php endif; ?>

        <!-- start main content -->
        <?php print($content) ?>
        <!-- end main content -->
        <?php print $feed_icons; ?>
        </td><!-- mainContent -->
    <?php if (!empty($right)): ?>
    <td class="sidebar" id="sidebar-right">
        <?php print $right ?>
    </td>
    <?php endif; ?>
  </tr>
</table>
<?php print $breadcrumb ?>
<div id="footer">
    <?php print $footer_message ?>
    <?php print $footer ?>
Validate <a href="http://validator.w3.org/check/referer">XHTML</a> or <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.
</div><!-- footer -->
 <?php print $closure;?>
  </body>
</html>

